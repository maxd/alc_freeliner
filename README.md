
# alcFreeliner #

The Aziz! Light Crew Freeliner is a live geometric animation software built with [Processing](https://www.processing.org). The documentation is a little sparse and the ux is rough but powerfull. Click the following image for a video demo.

[![IMAGE ALT TEXT](doc/videopic.png)](https://vimeo.com/246850149 "New Video")

[![IMAGE ALT TEXT](http://img.youtube.com/vi/ktxSKXwmmeU/0.jpg)](http://www.youtube.com/watch?v=ktxSKXwmmeU "Do you know Freeliner?")


## Installation & Launch ##
Here are some instructions on how to get started with freeliner.

1. Download and install Processing 4 from <https://processing.org/>.
2. Install these libraries:
	* [oscP5](http://www.sojamo.de/libraries/oscP5/) (available through Processing library Manager)
	* [Websockets](https://github.com/alexandrainst/processing_websockets) (available through Processing library Manager)
	* [Video](https://processing.org/reference/libraries/video/index.html) (available through Processing library Manager)
	* [SimpleHTTP](http://diskordier.net/simpleHTTPServer/) (download from site and extract to `sketchbook/Libraries`)
	* [Spout] Optional for windows users
	* [Syphon] Optional for MacOS users
3. Open and run `alc_freeliner/freeliner_sketch` with Processing
4. Once running you can point your browser to `http://localhost:8000/index.html` to access the browser interface
5. Increase Processing maximum available memory to 1024 in `File->Preferences->Running`.



### What is it?
This software is feature-full geometric animation software built for live projection mapping made with Processing. Development started in fall 2013.

Using a computer mouse cursor the user can create geometric shapes composed of line segments. These are created in groups, also known as segmentGroup. To facilitate this task the software has features such as centering, snapping, nudging, fixed length segments, fixed angles, grids, and mouse sensitivity adjustment.

Templates hold data that defines how a segmentGroup will be animated. There is 26 Templates, one per uppercase letter. The parameters are configurable via keys like ‘s’ for size, they can be set by typing a new numerical value or pressing ‘-‘ and ‘+’ (actually ‘-‘ and ‘=’). Each segmentGroup can have multiple templates.

This system of upper case and lower case letters allows the user to quickly modify the animations without clicking menus or other UI items.

### Philosophy ###

alcFreeliner must:
- Only require a computer with mouse, keyboard and display to run.
- Cross platform.
- Content Free, aside from fonts.
- Remain lightweight for old hardware.
- Focus on improvisation.

## How to freeLine ##
Point a projector at stuff, run the processing sketch. Navigate the space with your cursor, left click to add vertices, right click to remove them, and middle click to set their starting point.

The first few clicks puts down a special set of lines, they will display important info. The first three will display important info to make sense of this madness. Try to place them out of the way on a even surface so the text is legible. This is `[group : 0]` and thats all it does for now.

Now hit `n` to create a newItem and click around to place some lines. If you have made a closed shape, you can place a center. Now hit `A` to add renderer A.


<!-- #### Project Directory System -->

##### Tweaking Parameters
Most lowercase keys are linked with a parameter. For example `q` is for colorMode. Once you press `q` you can change the colorMode by pressing `-` or `=` (aka `+`) or by typing in a number and pressing `enter`. Some parameters are simple toggles. For example `g` enables and disables the grid, but you can also alter the grid size by tweaking the value. The `.` works in a similar fashion where you can enable/disable snapping and adjust the snapping distance.
See [auto generated keymap](freeliner_sketch/data/generated_doc/keymap.md) for a detailed list. All the different options for each parameter are found in the [auto generated mode list](freeliner_sketch/data/generated_doc/modes.md)

##### Tweaking parameters via OSC
Parameters related to rendering can be controlled via OSC. A message `/freeliner/tweak ABC q 2` will set templates A, B and C to red stroke. Typetag string string integer, the port can be set in the settings. You can find some PureData abstractions to get you started in `pd_patches`, great to quickly connect your midi controllers.

##### Snapping
The cursor can snap to various stuff. When snapping to points you can nudge them with the arrow keys, `shift` for bigger increments. Holding `ctrl` will momentarily disable snapping. If `ctrl` is pressed when snapped, you can right click drag the point. Snapping to segment middle you can use `backspace` to remove the segment. `.` toggles snapping and allows you to set the snapping distance.

##### Text Entry
`|` pipe begins a text entry and return key returns the text. This has a few uses. More later.

##### Toggle a template to groups with a template
Essentially adds a other template of your choice to any group who has the first template on the list.
Select two templates like `A` and `B`, press `ctrl-b`. All geometry that has `A` will now also have `B`.

##### Create a custom brush
Make a new segment group, set its center. Add a rendering template (`shift + a-z`) then hit (`ctrl + d`). That template will now have a custom brush corresponding to that segment group. You can then remove that template from the group and or remove all the segments of the group.

##### Copy parameters between templates
Unselect with `esc`, then select the template to copy, then select the template to paste into, and then press `ctrl-c`.   

##### Save and load
Now you can save a complete mapping, geometry and templates. Using `ctrl-s` and `ctrl-o` you can save and load the default files, `userdata/geometry.xml` and `userdata/templates.xml`.

##### Sequencing
16 step sequencing, may change. By tweaking the `>` character you can move through the steps. Steps are like items, they can have multiple templates. Press the `>` key then add a template or two then go to the next step with `=` (+) and onwards. It is recommended to use enabler mode 2.

##### Make Videos
The `*` character begins and stops a frame saving process. These `.tiff` images are saved in batches `capture/clip_N`. You can use Processing's movie maker tool to turn these into a video file. I recommend clearing `capture/` after making some clips.

##### Quitting
Freeliner can be quit with your systems shortcut to close a window, like `alt-f4` or with `cmd-w` on OSX.

##### Syphon
To use Syphon, install the Syphon Processing library and remove all the double slashes (`//`) in the `SyphonLayer.pde` file.

##### Spout
To use Spout, install the Spout Processing library and remove all the double slashes (`//`) in the `SpoutLayer.pde` file.

##### AutoDoc
Freeliner can now compile its own documentation. Due to the extendable nature of freeliner, extensions can get added to documentation automatically. Every template parameter.

## Current Bugs ##
Report one?

## SHOUTOUTS ##
* MrStar
* Zap
* Tats
* Quessy
* Jason Lewis
* BunBun
* ClaireLabs
* Bruno/Laika
* potluck crew
* [Robocut](http://robocutstudio.com/)
