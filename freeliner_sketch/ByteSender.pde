/**
 * ##copyright##
 * See LICENSE.md
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.4
 * @since     2016-10-17
 */


import java.io.*;
import java.net.*;

// for receiver
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

// A class to send byte arrays to lighting stuff.


public class ByteSender  {

    public ByteSender() {

    }

    public void connect(String _port, int _baud) {}
    public void connect(String _port) {}
    public void disconnect() {}
    public void sendData(byte[] _data, int _mode) {}
    public int getCount() {
        return 0;
    }
}

// send udp packets of variable length
public class ArtNetSender extends ByteSender {

    // String host;
    int port = 6454;

    int sequenceCount;
    // int artnetUniverseStart = 0;
    ArrayList<Host> hosts;
    Host artSyncHost;

    public ArtNetSender() {
        hosts = new ArrayList();
    }

    public void addHost(String _ip, int _start, int _count){
        Host _host =new Host(_ip, _start, _count);
        _host.connect();
        hosts.add(_host);
    }

    public void setArtSyncHostIP(String _ip) {
        artSyncHost = new Host(_ip, 0, 0);
    }

    public void sendData(byte[] _data, int _mode) {
        byte[][] _universes = splitUniverses(_data, _mode);
        for(int i = 0; i < _universes.length; i++) {
            for(Host _h : hosts){
                if(i >= _h.start && i <= _h.end){
                    sendUDP(_h, makeArtNetPacket(_universes[i], i));
                }
            }
        }
        sendArtSync(artSyncHost);
        sequenceCount++;
        sequenceCount %= 255;
    }


    public byte[][] splitUniverses(byte[] _data, int _mode) {
        // for RGB and RGBW
        int split = (_mode==3) ? 510 : 512;
        int _universeCount = _data.length/split;
        int _ind = 0;
        byte[][] _universes = new byte[_universeCount][512];
        for(int u = 0; u < _universeCount; u++) {
            for(int i = 0; i < split; i++) {
                _ind = (u*split)+i;
                if(_ind < _data.length) _universes[u][i] = _data[_ind];
                else _universes[u][i] = 0;
            }
            // if(u==2)printUniverse(_universes[u]);
        }
        return _universes;
    }

    // public void printUniverse(byte[] uni){
    //     int idx = 0;
    //     for(byte _b: uni){
    //         idx++;
    //         print(idx+":"+_b+", ");
    //         if(idx%16==0)println();
    //     }
    //     println("=============================");
    // }

    public byte[] makeArtNetPacket(byte[] _data, int _uni) {
        long _size = _data.length;
        byte _packet[] = new byte[_data.length+18];
        _packet[0] = byte('A');
        _packet[1] = byte('r');
        _packet[2] = byte('t');
        _packet[3] = byte('-');
        _packet[4] = byte('N');
        _packet[5] = byte('e');
        _packet[6] = byte('t');
        _packet[7] = 0; // null terminated string
        _packet[8] = 0; //opcode
        _packet[9] = 80; //opcode
        _packet[10] = 0; //protocol version
        _packet[11] = 14; //protocol version
        _packet[12] = (byte)sequenceCount; //sequence
        _packet[13] = 0; //physical (purely informative)
        _packet[14] = (byte)_uni;//(byte)(_uni%16); //Universe lsb? http://www.madmapper.com/universe-decimal-to-artnet-pathport/
        _packet[15] = 0;//(byte)(_uni/16); //Universe msb?
        _packet[16] = (byte)((_size & 0xFF00) >> 8); //length msb
        _packet[17] = (byte)(_size & 0xFF); //length lsb

        for(int i = 0; i < _size; i++) {
            _packet[i+18] = _data[i];
        }
        return _packet;
    }

    public void sendArtSync(Host _host){
        byte _packet[] = new byte[14];
        _packet[0] = byte('A');
        _packet[1] = byte('r');
        _packet[2] = byte('t');
        _packet[3] = byte('-');
        _packet[4] = byte('N');
        _packet[5] = byte('e');
        _packet[6] = byte('t');
        _packet[7] = 0; // null terminated string
        _packet[8] = 0; //opcode
        _packet[9] = 82; //opcode
        _packet[10] = 0; //protocol version
        _packet[11] = 14; //protocol version
        _packet[12] = 0; //protocol version
        _packet[13] = 0; //protocol version
        sendUDP(_host, _packet);
    }

    public void sendUDP( Host _host, byte[] _data) {
        if(_data == null) return;
        DatagramPacket packet = new DatagramPacket(_data, _data.length,
                _host.address, port);
        try {
            _host.dsocket.send(packet);
        }
        catch(Exception e) {
            println("failed to send");
            _host.connect();
        }
    }

}


// host class used by artnet sender to define hosts 
// with ranges of universes to send data to
class Host{
    String ip;
    int start;
    int end;
    InetAddress address;
    DatagramSocket dsocket;
    Host(String _ip, int _start, int _count){
        ip = _ip;
        start = _start;
        end = start+_count;
    }
    void connect(){
            // host = _adr;
            try {
                address = InetAddress.getByName(ip);
                dsocket = new DatagramSocket();
                // println("[artnet] host connected: "+ip);
            }
            catch(Exception e) {
                println("[artnet] could not connect to host: "+ip);
                // exit();
            }
    }
}


public class ArtNetReceiver {
    private BlockingQueue<DatagramPacket> packetQueue;
    private Thread receiverThread;
    private boolean running;

    private DatagramSocket socket;
    private byte[] packetBuffer;
    // different artnet universes write to this frame buffer
    // the buffer is then merged with the fancyFixtures framebuffer
    private byte[] frameBuffer;
    int port = 6454;
    final int headerSize = 18;
    // public dmxData[][]
    public ArtNetReceiver() {
        println("[artnet] receiver-merger initialisation");
        packetBuffer = new byte[1024]; // Buffer to store received packet data
        frameBuffer = new byte[512];
        
        packetQueue = new LinkedBlockingQueue<DatagramPacket>();
        running = false;

    }
    public void setSize(int _frameBufferSize){
        frameBuffer= new byte[_frameBufferSize];
    }
    public void start(){
        try {
            socket = new DatagramSocket(port); // Create a socket to listen on the specified port
            if (!running) {
                running = true;
                receiverThread = new Thread(new ReceiverRunnable());
                receiverThread.start();
            }
        } catch (Exception e) {
            println("faiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiled");
            println(e);
        }
    }
    
    public void stop() {
        if (running) {
            running = false;
            receiverThread.interrupt();
        }
        println("CLOSING SOOOOOOOOOOOOOOOCKET");
        if(socket != null){
            socket.close();
        }
    }

    // @Override
    // protected void finalize() throws Throwable {
    //     try {
    //         System.out.println("[artnet-receiver] closing");
    //         stop();
    //         // Perform any necessary cleanup operations here
    //     } finally {
    //         super.finalize();
    //     }
    // }

    private class ReceiverRunnable implements Runnable {
        @Override
        public void run() {
            while (running) {
                try {
                    DatagramPacket packet = new DatagramPacket(packetBuffer, packetBuffer.length);
                    socket.receive(packet);
                    packetQueue.put(packet);
                } catch (Exception e) {
                    // Handle or log the exception as needed
                }
            }
        }
    }

    public void update() {
        while(packetQueue.peek() != null) {
            parsePacket(packetQueue.remove());
        }
    }

    private boolean parsePacket(DatagramPacket packet){
        if(packet == null){
            return false;
        }
        if (packet.getLength() >= 8 && new String(packet.getData(), 0, 7).equals("Art-Net")) {
            int opcode = packet.getData()[8] | packet.getData()[9] << 8; // Get the opcode of the packet
            // Handle different opcodes
            switch (opcode) {
                case 0x5200:
                    // we end up receiving our own artsync packet...
                    break;
                case 0x5000: // ArtDmx packet
                    int universe = packet.getData()[14] | packet.getData()[15] << 8; // Get the universe number
                    int dataLength = packet.getData()[17] | packet.getData()[16] << 8; // Get the data length
                    handleDmxData(universe, packet.getData(), dataLength);
                    return true;
                // Add more cases to handle other Art-Net packet types if needed
                default:
                    // println(String.format("Unsupported Art-Net packet: 0x%08X \n",opcode));
                    break;
            }
        }
         
        return false;
    }


    private void handleDmxData(int universe, byte[] data, int length) {
        int universeOffset = universe*510; //might be 510?
        // todo, get rid of stale data when not receiving?
        for (int i = 0; i < length; i++) {
            if(i+universeOffset < frameBuffer.length){
                frameBuffer[i+universeOffset] = data[i+headerSize];
                // if(i< 8) print(data[i+headerSize]+", ");
            }
        }
    }


}